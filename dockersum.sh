#!/bin/bash

echo '-----------------------------------------------'
echo '| CONTAINERS |'
echo '------------'
docker container ls $*
echo '-----------------------------------------------'
echo '|  SERVICES  |'
echo '--------------'
docker service ls
echo '-----------------------------------------------'
echo '|  NETWORKS  | '
echo '--------------'
docker network ls
echo '-----------------------------------------------'
echo '|  VOLUMES  | '
echo '------------'
docker volume ls
echo '-----------------------------------------------'
exit 0