#!/bin/bash

function show_usage() {
        echo 'USAGE:'
        echo "    $0 containerId"
        echo ''
        echo 'SYNOPSIS:'
        echo '    Show which host interface is used by container defined by containerId'
        echo ''
        echo 'ARGUMENTS:'
        echo "    containerId\tThe container ID you are interested in"
        echo ''
        echo 'OPTIONS:'
        echo "    -h, --help\tShow this help message and exit"
}

declare -a positional=()
while [ $# -gt 0 ]
do
        case $1 in
                -h|--help) show_usage; exit 0;;
                *) positional+=("$1"); shift;;
        esac
done
set -- "${positional[@]}"

containerId="${positional[0]}"

echo "Container ID = ${containerId}"

declare -a containerIfList=($(docker exec "${containerId}" bash -c 'ls -1 /sys/class/net/' | grep -v "^lo$"))

for iface in ${containerIfList[@]}
do
        echo "Treating interface ${iface} in ${containerId}"
        iflink=$(docker exec "${containerId}" bash -c "cat /sys/class/net/${iface}/iflink")
        echo "iflink = ${iflink}"
        ifindexPath=$(grep -l ${iflink} /sys/class/net/*/ifindex)
        echo "ifindexPath = ${ifindexPath}"

        phyIface=$(sed -e 's%^/sys/class/net/\([^/]*\)/.*$%\1%' <<< "${ifindexPath}")
        #phyIface=$(ip addr show | grep "^${iflink}" | cut -d' ' -f2 | cut -d'@' -f1)
        echo "phyIface = ${phyIface}"

        echo -e "Interface ${iface} bound to ${phyIface}"
	master=$(cat "/sys/class/net/${phyIface}/master/uevent" | grep 'INTERFACE' | cut -d'=' -f2)
        
	echo -e "Interface ${phyIface} is bridged on master ${master}"

	echo ''
	ip addr show dev "${phyIface}"
	ip addr show dev "${master}"
	echo ''
done

exit 0
